(* Example of error management with polymorphic variants + a message field.

   Polymorphic variants allow to define errors very lightly at the location
   where errors are raised. The type-checker builds a union of all errors
   that can occur, forcing one to check all possible errors when pattern-matching
   (but it does not warn if one checks for an error that cannot actually happen).

   Some drawbacks of using polymorphic variants for error handling are:
   1. the type of functions need to list all possible errors that can occur;
   2. it does not seem possible to have a global registration mechanism
      in order to have a single "to_string" function like we do with tzresult.

   Arguably, 1. is not actually a drawback but the main strength of this approach:
   one has to document, in the type of functions, which errors they can return.
   This documentation is very valuable. And the type-checker uses this documentation
   to force you to handle all errors.

   The message field is used to solve drawback 2. If each error comes with its
   error message, the "to_string" function is trivial. However, this is not completely
   satisfying either for some cases: we have "to_string", but not "of_string"
   (i.e. we can encode but not decode since there is no registry of all errors
   that can happen).

   Note that the message field can be replaced by a data-encoding field instead
   (probably of type ['a Data_encoding.case]). But you'll see in the examples below
   that the message field does not make the solution much heavier: it is still easy
   to define errors on-the-fly. But if we have to define an encoding it becomes much
   more complicated. We could experiment with registering functions like we did
   for [Internal_event.Simple] to make error definitions more lightweight though.
   But it would still not solve the problem of having a global decoding function for
   all errors.

   On the other hand, one could argue that a global decoding function is rather limited:
   it means that we accept that all RPCs can return absolutely any error.
   We don't know, for example, that "GET /config" cannot return "protocol error".
   With a global decoding function the list of errors that can occur is not documented,
   at least not in the types (so there is no hope of the type-system helping there).

   A global decoding function is thus mostly useful when the only thing a client does
   when an error occur is to display this error and then give up. For this particular
   use case, sending a message field could provide the same functionality. Maybe there
   are cases where a client acts differently depending on the error an RPC return;
   if those cases are limited, one can just define an encoding for those particular RPCs.
   Having a different error encoding for each RPC brings value: callers know exactly
   what to expect. *)

(* Errors are composed of an error code and an error message.
   In C programs the error code would be an int.
   In OCaml it can be anything, but here we use polymorphic variants
   so that the monadic bind operator can gather all errors that can happen
   in a single type. Nothing prevents you from using something else, but then
   when you bind, but the left-hand side and the right-hand side must use
   the same error type. *)
type 'e error = { code: 'e; message: string }

(* The result type we use: all error cases come with messages. *)
type ('a, 'e) r = ('a, 'e error) result

(* Return an error. *)
let error code message = Error { code; message }

(* A version which accepts formatting strings for messages.
   May not be a good idea since formatting takes ressources and if we discard
   the error message later those ressources have been used for no reason.
   A more efficient option would be to turn [message] into a list of strings,
   since as you'll see below we often use [errorf] to add some context to an
   existing error message. Instead we could add context using cons [::]. *)
let errorf code x = Printf.ksprintf (error code) x

(* The bind operator. *)
let (let+) r f =
  match r with
    | Ok x -> f x
    | Error _ as e -> e

(* Let's define a small system library which can return some errors. *)
module System:
sig

  type file

  (* The [open_file] function type states that it can return two errors:
     file not found, and permission denied. The polymorphic variant type is open,
     with >, so that the type can be unified with other polymorphic variant types
     when using the monadic bind.

     Some errors can have parameters. Here, file not found comes with the filename. *)
  val open_file: string -> (file, [> `file_not_found of string | `permission_denied ]) r

  (* The [read_line] function chose to return end of file as an error.
     It could be argued that end of file is not an error, and that the result
     type should be [(string option, [> `disk_exploded ]) r] instead.
     This distinction would be very important with an open type like tzresult;
     but here, it does not matter as much since both types contain the same information.
     In other words, mistakes when choosing what is or is not an error are less costly
     with this approach. *)
  val read_line: file -> (string, [> `end_of_file | `disk_exploded ]) r

end = struct

  type file = unit

  let open_file filename =
    if not (Sys.file_exists filename) then
      errorf (`file_not_found filename) "file not found: %s" filename
    else if Random.bool () then
      error `permission_denied "permission denied"
    else
      Ok ()

  let read_line () =
    if Random.bool () then
      error `end_of_file "end of file"
    else if Random.bool () then
      error `disk_exploded "disk exploded"
    else
      Ok "data"

end

(* Let's use our library to make another one. *)
module Config:
sig

  type t = { rpc_port: int; p2p_port: int }

  (* This function uses [open_file] and [read_line].
     So any error of [open_file] or [read_line] can be returned.
     Additionally, [`parse_error] can also happen. *)
  val parse_file: string ->
    (t, [> `file_not_found of string | `permission_denied | `end_of_file
        | `disk_exploded | `parse_error ]) r

  (* This function is similar to [parse_file], but recovers some errors.
     It does not know how to recover from disk explosions though,
     nor from corrupted configuration files. *)
  val try_to_parse_file: string -> (t, [> `disk_exploded | `corrupted ]) r

end = struct

  type t = { rpc_port: int; p2p_port: int }

  (* Let's make a version of [int_of_string] which returns [`parse_error]
     if the string does not denote an integer, instead of returning [None]
     or raising an exception. *)
  let parse_int string =
    match int_of_string_opt string with
      | Some i -> Ok i
      | None -> errorf `parse_error "parse error: invalid integer: %S" string

  let parse_file filename =
    let+ file = System.open_file filename in
    let+ line = System.read_line file in
    let+ rpc_port = parse_int line in
    let+ line = System.read_line file in
    let+ p2p_port = parse_int line in
    Ok { rpc_port; p2p_port }

  (* This function demonstrates a useful pattern: how to recover from some errors
     and remove them from the resulting type, or group some errors together
     to simplify the interface (here both "end of file" and "parse error"
     become "corrupted"). *)
  let try_to_parse_file filename =
    match parse_file filename with
      | Ok file ->
          Ok file
      | Error { code = (`file_not_found _ | `permission_denied); _ } ->
          (* Recover: use default values. *)
          Ok { rpc_port = 8732; p2p_port = 9732 }
      | Error { code = `disk_exploded; _ } as e ->
          (* Propagate this error as is. *)
          e
      | Error { code = (`end_of_file | `parse_error); message } ->
          (* Group those two error cases as a single one. *)
          errorf `corrupted "corrupted configuration file: %s" message

end

module Node:
sig

  (* This function reads a config file and embeds the errors that can happen while
     doing so in a larger category: [`failed_to_read_config_file].
     Then it tries to start the RPC and P2P servers and can also fail to do that.
     Embedding config file errors allows the caller to know that [`corrupted],
     for instance, happened while reading the config file and not while doing something else.
     The caller can also easily match on "any error that occurs
     while reading the config file" if it does not care which particular error occurred.
     Or, it can act differently if the config file is corrupted or if the disk exploded. *)
  val init: unit ->
    ( unit,
      [> `failed_to_read_config_file of [ `disk_exploded | `corrupted ]
      | `failed_to_start_http_server ]) r

end = struct

  let init () =
    match Config.try_to_parse_file "config.json" with
      | Error { code; message } ->
          (* We don't have to explicitely list all cases for [code] here.
             The mli already does that.
             So if a new error is added to [Config.try_to_parse_file],
             we'll still have a type-error here to fix, forcing us to make sure
             we handle this error correctly. *)
          errorf (`failed_to_read_config_file code) "failed to read config file: %s" message
      | Ok { rpc_port; p2p_port } ->
          let+ () =
            if Random.bool () then
              errorf `failed_to_start_http_server "failed to start RPC server on port %d"
                rpc_port
            else
              Ok ()
          in
          let+ () =
            if Random.bool () then
              errorf `failed_to_start_http_server "failed to start P2P server on port %d"
                p2p_port
            else
              Ok ()
          in
          Ok ()

end

module Main:
sig

  val run: unit -> unit

end = struct

  (* Program entrypoints can print error messages on stderr. *)
  let run () =
    match Node.init () with
      | Error { message; _ } ->
          Printf.eprintf "Error: %s\n%!" message;
          exit 1
      | Ok () ->
          ()

end
