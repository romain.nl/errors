# Error Management in OCaml

## Exceptions

Pros:
- built-in language support = the natural, naive, easy way to handle errors
- easy to propagate
- easy to catch
- can register encodings for each exception

Cons:
- types do not declare which exceptions may happen
  - so no documentation...
  - ...and no help from the type-checker
- does not work well with concurrency

Exceptions are not just for errors by the way.
They can be used to manage control-flow.
If done locally (i.e. raised and catched in the same unit of abstraction,
e.g. a function a module) it leaves less room for mistakes.

## Result With An Extensible Sum Type For Errors

This is `tzresult` for instance.

It is basically equivalent to `('a, exn) result` since `exn` is an extensible sum type itself.

Pros:
- rather easy to propagate with the bind operator of the error monad
- rather easy to catch with `match with`, although a bit more verbose than `try with`
- a function which returns an error has to declare in its type that it returns an error
- works well with concurrency
- can register encodings for each error

Cons:
- a function which returns an error does not declare which errors it can return
- the type-checker does not help you to not forget a particular error

## Result With Sum Types For Errors

This consists in having each function declare a (non-extensible) type with the list of errors
it can return. For instance:

    type open_file_error = No_space_left_on_device | File_not_found | Permission_denied
    val open_file: ... -> (unit, open_file_error) result

Pros:
- functions document which errors they can return very well
- the type-checker ensures you don't forget to handle those errors
- still easy to catch with `match with` and you don't even need the `_ ->` case
- still works well with concurrency
- very flexible: each function does whatever it wants
  - some may just return `option`s
  - some may embed traces in their errors, e.g.
    `type block_read_error = Failed_to_open_file of open_file_error | Something_else`
- works in any language, including the simplest ML you can think of

Cons:
- cannot propagate errors in a generic way
- lots of boilerplate needed: each function call basically needs a `match with` around it,
  and each function needs to document its errors
- cannot have one encoding for all errors: need one encoding per error type
  (although one could argue that it's not actually more verbose in the end,
  you just have to pick the right encoding when you want to encode)

Note that you don't actually need error types to be sum types.
You can just as well use `int`, or `string`, or records, depending on the function.

## Result With Polymorphic Variants For Errors

This is very similar to results with (non-extensible) sum types:

    type open_file_error = [ `no_space_left_on_device | `file_not_found | `permission_denied ]
    val open_file: ... -> (unit, [> open_file_error ]) result

The `[>` is important, without it you cannot propage errors easily with a bind operator.

Pros and cons are basically the same as sum types since polymorphic variants are an
extension of sum types. However:
- it becomes easy to propagate errors since the type-checker makes unions of
  polymorphic variants automatically, so you can have a bind operator
- you don't have to declare types, you can write

    val open_file: ... -> (unit, [> `no_space_left | `file_not_found ])

  directly if you want
- functions can share some errors, which is both:
  - good when it makes sense
  - bad if you want to distinguish between the two (in that case you need the caller
    to rename the errors)

It still requires you to write one encoding per function for errors.
And it still requires some boilerplate, although some useful combinators are possible.

## Result With Errors Carrying Messages

The three result cases above (extensible sum types, non-extensible types, and polymorphic
variants) have in common that errors are represented by a "code".
In languages such as C, error codes are often integers.
In OCaml we can use sum types to give a name to those enums and get help from
the type-checker when pattern-matching, but it's still a way to represent an error code.

But one can have errors carry more than an error code.
Errors can carry metadata.
In particular, they can carry their encoding.
If all errors use the same way to carry their encoding, we can have a generic
function to encode errors.

The simplest way to do that is to define a type such as:

    type 'a error = {
      code: 'a;
      message: string;
    }

To encode or display the error you just use the `message` field.

This means that one does not write:

    Error no_space_left_on_device

but:

    Error { code = no_space_left_on_device; message = "no space left on device" }

which is still rather convenient.
It is made easier with an `error` function:

    let error code message = { code; message }

Now you just write:

    error no_space_left_on_device "no space left on device"

A simple extension to this approach is to have `message` be a list of strings:

    type 'a error = {
      code: 'a;
      message: string list;
    }

This allows you to carry structured traces when embedding errors:

    match open_file () with
      | Error { code; message } ->
          error Failed_to_read_block ("failed to read block" :: message)
      | Ok () ->
          ...

Side-note: here `Failed_to_read_block` can carry more information like this:

    match open_file () with
      | Error { code; message } ->
          error (Failed_to_read_block (Open_file_failed code))
            ("failed to read block" :: message)
      | Ok () ->
          ...

depending on what is needed by the caller.
With polymorphic variants it is even easier:

    match open_file () with
      | Error { code; message } ->
          error (`failed_to_read_block code)
            ("failed to read block" :: message)
      | Ok () ->
          ...

## Result With Errors Carrying Encodings

The `message` field only allows one to display an error, not really to
transmit it over the network. One can always transmit the string, and it is often
sufficient, but if the remote peer needs to pattern-match on the error,
we want to send the `code` field itself. So an alternative definition for `error` can be:

    type 'a error = {
      code: 'a;
      encoding: 'a Data_encoding.t;
    }

It is much harder to quickly define a new error if one has to define
its encoding compared to just having to write a message string though.

Another problem is that while this allows to transmit the value easily,
it does not solve the problem of the remote peer decoding the error.
Indeed, this solves the problem by attaching runtime metadata to errors,
but here we need static metadata. For each RPC we need to have a static
encoding for this particular RPC.

This brings us back to the case of results with sum types for errors,
where we had to define an encoding for each error, and we would simply
choose the right encoding when defining RPCs. So one could argue
that this `error` type does not bring much value if one wants to transmit
errors between processes.

## Result With Errors Carrying Easy-To-Serialize Values

There is probably a design space around a compromize between the last two sections,
where instead of a rather limited `message: string list` and of a very complicated
`encoding: 'a Data_encoding.t`, we could have semi-structured errors.

I would argue that when we pattern-match on errors to decide what to do,
we are most often interested only in a value which boils down to an integer.
For instance, in the error "block BL123456789 does not exist", we are only
interested in the fact that "a block does not exist", not in the "BL123456789" value
itself, which is only useful for display purposes.

So maybe there is a way to define a semi-structured `error` type such as:

    type 'a error = {
      code: 'a;
      tag: int;
      parameters: (string * string) list;
    }

which would be registered with a function to pretty-print a couple `(tag, parameters)`.

One may also be able to add a `trace` field with a list of `(tag, parameters)` couples.

The key point is that the type of `code` is `'a`, so each function can use
its own error type and so we can pattern-match on `code` with help from the type-checker;
but we can still transmit errors.

If the client receives such an error it can pattern-match on the tag, but we would
prefer to decode the tag into a `code` so that we can have typed pattern-matching.
I feel like it should be possible to take a list of possible registered errors and
make a decoder for this list of errors so that we can do that.
